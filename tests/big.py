import cv2
import numpy as np
import subprocess
from multiprocessing import Pool
from os import listdir

#intentemos añadir un archivo grande a ver que pasa
#será posible leer un videocapture en vivo ignorando todos los frames que no nos interesan? 
#será más rápido
#primero vemos que tantos frames existen
"""
vid=cv2.VideoCapture('video.mp4')
num=vid.get(cv2.CAP_PROP_FRAME_COUNT)

h=True
indx=np.arange(0,vid.get(cv2.CAP_PROP_FRAME_COUNT))[::-1]
print(indx)
#mostramos el frame solo si este se encuentra indexado
for k in indx:
    print(k)
    vid.set(1,k)
    h,frame=vid.read()
    cv2.imshow('frames',frame)
    cv2.waitKey(2)
cv2.destroyAllWindows()
"""
"""
while True:
    if n==indx[i]:
        h,frame=vid.read()
        cv2.imshow('img',frame)
        cv2.waitKey(2)
    else:
        vid.grab()
cv2.destroyAllWindows()
"""
#lei que set(1,) puede usarse para especificar el frame que se quiere extraer, quiero ver si puedo extraer un solo frme y procesarlo y ver que pasa


#en otro intento, separo el video en trozos de por ejemplo 3 segundos, luego los leo con multiprocessing
#sigo teniendo problemas por errores de memoria :(
"""
subprocess.call(
        ['ffmpeg','-i','video.mp4','-c','copy','-map','0','-segment_time','3','-f','segment','tempframes/output_video%03d.mp4'])
def leer(x):
    vid=cv2.VideoCapture(x)
    h,frame=vid.read()
    frames=np.array([frame])
    print('leyendo frame')
    while h:
        h,frame=vid.read()
        frame=np.array([frame])
        if h:
            frames=np.concatenate((frames,frame))
        else:
            break
    return frames

p=Pool(5)
datos=p.map(leer,['tempframes/'+k for k in listdir('tempframes')])
print(datos)
"""

#otra idea que se me ocurre es usar ffmpeg para que me devuelva los frames que necesito y ya alv
#pero como me devuelve el frame en el segundo 00:00:00 entonces le tengo que especificar que segundo es el frame que necesito y así

vid=cv2.VideoCapture('video.mp4')
fps=vid.get(cv2.CAP_PROP_FPS)
num=vid.get(cv2.CAP_PROP_FRAME_COUNT)
#cuanto dura el video?
total=num/fps
print(fps,num,total)
for k in range(int(num)):
    subprocess.call(['ffmpeg','-y', '-i', 'video.mp4', '-ss', '00:00:0{}'.format(k/fps), '-vframes', '1', 'thumb.jpg'])
    frame=cv2.imread('thumb.jpg')
    cv2.imshow('frame',frame)
    cv2.waitKey(2)



