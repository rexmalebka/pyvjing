import cv2
from time import sleep
from threading import Thread
import numpy as np
import readline 
from os.path import exists

class clase():
    vel=1
    run=True
    video=cv2.VideoCapture('salida.mp4')
    i=0
    f=-1
    p=1
    ischanged=False

    #definimos las funciones del reproductor
    def velocity(t=vel):
        try:
            t=float(t)
            assert t>0
            clase.vel=t
            clase.ischanged=True
        except Exception as e:
            print('  (!) {}'.format(e.args))

        return "vel. muestreo -> "+str(t)
    
    def end(*_):
        clase.run=False
        clase.ischanged=True
        return "fin."

    def load(nombre,*_):
        if exists(nombre):
            clase.video=cv2.VideoCapture(nombre)

            # cargo el video para loopearse para siempre
            print('cargando video...')
            h,frame=video.read()
            clase.frames=np.array([frame])
            while h:
                h,frame=video.read()
                frame=np.array([frame])
                if h:
                    #print(frame.shape,frames.shape)
                    clase.frames=np.concatenate((frames,frame))
                else:
                    break

    def loop(args,*_):
        inicio=0
        fin=-1
        paso=1

        args=args.split(',')
        if len(args)==1:
            try:
                inicio=int(args[0])
                clase.ischanged=True
            except Exception as e:
                print('  (!) {}'.format(e.args))
                pass
            fin=-1
            paso=1
        elif len(args)==2:
            try:
                inicio=int(args[0])
                fin=int(args[1])
                clase.ischanged=True
            except Exception as e:
                print('  (!) {}'.format(e.args))
                pass
            fin=int(args[1])
            paso=1
        elif len(args)==3:
            try:
                inicio=int(args[0])
                fin=int(args[1])
                paso=int(args[2])
                clase.ischanged=True
            except Exception as e:
                print('  (!) {}'.format(e.args))
                pass
        try:
            clase.frames[inicio:fin:paso]
        except Exception as e:
            print('  (!) {}'.format(e.args))
            inicio=0
            fin=-1
            paso=1

        clase.i=inicio
        clase.f=fin
        clase.p=paso
        return "loop -> [{} : {} : {}]".format(inicio,fin,paso)

    def hsv(*_):
        #esta funcion convierte en hsv los frames
        #clase.frames[clase.i:clase.f:clase.p]=cv2.cvtColor
        #clase.frames[clase.i:clase.f:clase.p]=
        pass
    proc={
            'vel':velocity,
            'end':end,
            'loop':loop
            }
    funct={}

    load('salida.mp4')
    def commands():
        while clase.run:
            inst=input('> ')
            #es una función del editor?
            for k in [l for l in inst.split('&') if l!='']:
                func=[l for l in k.split(' ') if l!='']
                if func[0][0]=='#':
                    func[0]=func[0][1:]
                    if clase.proc.get(func[0])!=None:
                        print(clase.proc.get(func[0])(*func[1:]))

    def mostrar():
        while clase.run:
            for n,frame in enumerate(clase.frames[clase.i:clase.f:clase.p]):
                if clase.run==False or clase.ischanged:
                    break
                cv2.imshow('img',frame)
                cv2.waitKey(2)
                sleep(clase.vel)
            clase.ischanged=False

Thread(target=clase.commands).start()
Thread(target=clase.mostrar).start()
