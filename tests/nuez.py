import cv2
from multiprocessing import Pipe
from threading import Thread
from random import randint
import readline
import numpy as np


class edit():
 
    def blur(par,chi,umb,*_):
        umbm=5
        umbM=5
        arg=umb.split(',')
        if len(arg)==1:
            umbm=arg[0]
            umbM=arg[0]
        elif len(arg)==2:
            umbm=arg[0]
            umbM=arg[1]

        try:
            umbm=int(umbm)
            umbM=int(umbM)
            assert umbm>0 or umbM>0
        except Exception as e:
            print(e)
        chi.send(cv2.blur(par.recv(),(umbm,umbM)))

    def hsv(par,chi,*_):
        chi.send(cv2.cvtColor(par.recv(),cv2.COLOR_BGR2HSV))

    def erosion():
        pass

    proc={
            'blur':blur,
            'hsv':hsv
            }




class rep():
    def end(*_):
        vj.changed=True
        vj.run=False

    def mostrar(par,*_):
        while vj.run:
            cv2.imshow('frame',par.recv())
            cv2.waitKey(2)
            while vj.pause:
                pass
            if vj.run==False:
                break

    def load(chi,nombrevideo,*_):
        video=cv2.VideoCapture(nombrevideo)
        #metemos al contenedor el objeto de video capture
        vj.numframes=video.get(cv2.CAP_PROP_FRAME_COUNT)
        vj.indx=np.arange(1,vj.numframes)
        n=0
        while vj.run:
            #n=randint(1,fps-1)
            for k in vj.indx[vj.i:vj.f:vj.p]:
                video.set(1,k) 
                while vj.pause:
                    pass
                chi.send(video.read()[1])
                if vj.changed:
                    break
            vj.changed=False

    def loop(arg,*_):
        i,f,p='','',''
        args=arg.split(',')
        if len(args)==3:
            i=args[0]
            f=args[1]
            p=args[2]
        elif len(args)==2:
            i=args[0]
            f=args[1]
        elif len(args)==1:
            i=args[0]

        try:
            i=None if i=='' else int(i)
            f=None if f=='' else int(f)
            p=None if p=='' else int(p)
            assert len(vj.indx[i:f:p])>0
        except Exception as e:
            print(e)
        vj.i=i
        vj.f=f
        vj.p=p
        vj.changed=True
        return (vj.i,vj.f,vj.p)

    def pause(*_):
            vj.pause=not vj.pause
    
    proc={
            'load':load,
            'loop':loop,
            'pause':pause,
            'end':end
            }



class vj():
    run=True
    pause=False
    changed=False

    i=None
    f=None
    p=None

    act=[]


    def parser():
        while True:
            inpt=input('> ')
            
            #detect secuential instructions first
            instructions=[]
            for inst in inpt.split('&'):

                #remove whitespaces in the begining and the end of the instruction
                inst=inst.lstrip(' ').rstrip(' ')

                #detect parallel instructions inside secuencial instructions
                anid=[]
                for pinst in inst.split('|'):
                    
                    #remove whitespaces in the begining and the end of the parallel instruction
                    pinst=pinst.lstrip(' ').rstrip(' ')

                    fun=[k for k in pinst.split(' ') if k!=' ']
                    f={}
                    if fun[0][0]=='#':
                        f['mode']='rep'
                        f['name']=fun[0][1:]
                    else:
                        f['mode']='edi'
                        f['name']=fun[0]

                    #separate arguments into a list
                    f['args']=''.join(fun[1:]).split(',')
                    anid.append(f)
                instrucciones.append(anid)
            print(instrucciones)

    def process(par,chi):
        while vj.run:
            if len(vj.act)==0:
                chi.send(par.recv())
            else:
                for k in vj.act:
                    if len(k)>1:
                        edit.proc.get(k[0])(par,chi,*k[1:])
                    else:
                        edit.proc.get(k[0])(par,chi)
            vj.changed=False

            while vj.pause:
                pass




rep()
par,chi=Pipe()
parp,chip=Pipe()
"""
Thread(target=vj.commands).start()
Thread(target=vj.process,args=(par,chip,)).start()
Thread(target=rep.load,args=(chi,'video.mp4')).start()
Thread(target=rep.mostrar,args=(parp,)).start()
"""
