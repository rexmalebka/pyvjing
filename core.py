import cv2
from multiprocessing import Pipe
from threading import Thread
import numpy as np
from time import sleep, time
import logging
import readline
from kidney import fx
from kidney import rep
#intentaré cargar un buffer

"""
carga 20 frames
se los pasa al mostrador
el mostrador loopea la lista y muestra 
cuando inicia la reproducción, le manda una señal al buffer para que lea los sguientes 100 elementos y así infinitamente

comunicacion hijo-padre
calcular indices desde inicio hasta fin
o desde fin-inicio si el paso es negativo

"""
'''
class rep():
    framesb=[]
    bufsize=20
    functions=[]

    def pred():
        return 'a'


    def load(name,bufferparent,showchild):

    def show(fps,bufferchild,showparent):
        while True:

            #mientras se leen los buffers se espera
            while showparent.recv():
                if len(clase.framesb)!=0:
                    for n,frame in enumerate(frames):
                        cv2.imshow('frame',frame)
                        sleep(1/fps)
                        cv2.waitKey(2)
                pass
            #envia la señal para evitar se que bloquee el buffer
            bufferchild.send(False)
            #una vez terminada la lectura, recupera los cuadros leidos
            #frames=framesparent.recv()
            frames=clase.framesb.copy()

            #los numera y los muestra
            for n,frame in enumerate(frames):
                cv2.imshow('frame',frame)
                sleep(1/fps)
                cv2.waitKey(2)

                #si es el cuadro 50 envia la señal para que haga la lectura del buffer
                if n==1:
                    bufferchild.send(True)
'''
class vj():
    fxf=[]
    repf=[]
    def init():
        #creamos las pipas y los threads desde aqui,
        bufferparent,bufferchild=Pipe()
        showparent,showchild=Pipe()
        
        #Thread(target=clase.load,args=(cap,bufferparent,showchild)).start() 
        #print('thread buffer cargado')

        #Thread(target=rep.show,args=(fps,bufferchild,showparent)).start()
        Thread(target=vj.parser).start()

    def parser():
        while True:
            instructions=[]
            repf={}
            fxf=[]
            repl=rep.repl.keys()
            fxl=fx.fxl

            inpt=input('> ')
            
            #detect sequential instructions first
            
            for inst in inpt.split('&'):

                #remove whitespaces in the begining and the end of the instruction
                inst=inst.strip(' ')

                #detect parallel instructions inside secuencial instructions
                nested=[]
                for pinst in inst.split('|'):

                    #remove whitespaces in the begining and the end of the parallel instruction
                    pinst=pinst.strip(' ')

                    #remove spaces betweeen char and funcion name
                    if len(pinst)>1 and pinst[0]=='#':
                        pinst='#'+pinst[1:].lstrip(' ')

                    #separates function name and args
                    fun=[k for k in pinst.split(' ') if k!=' ' or k!='']
                    f={}
                    if len(fun[0])>0 and fun[0][0]=='#':
                        #check if it's a rep function
                        f['mode']='rep'
                        f['name']=fun[0][1:]
                    elif len(fun[0])>0:
                        #check if it's an fx function
                        f['mode']='fx'
                        f['name']=fun[0]
                    else:
                        #else nothing
                        f['name']=''
                        f['mode']=''

                    #separate arguments into a list
                    f['args']=''.join(fun[1:]).split(',')
                    nested.append(f)

                #append nested instructions
                instructions.append(nested)

            #this is for coincidence check
            coinc={}
            nf=False
            for seq in instructions:
                s=[]
                for nest in seq:

                    if nest['mode']=='fx':
                        if nest['name'] in fxl:
                            #check if fx name exists in fxl
                            s.append(nest)
                        else:
                            nf=True
                            #check if coincidence already added, else add it
                            if not nest['name'] in coinc.keys():
                                coinc[nest['name']]={}
                            coinc[nest['name']].update({'fx':set()})

                            #check if name exists in any of the str of names or the other way
                            for k in fxl:
                                if (nest['name'] in k) or (k in nest['name']):
                                    coinc[nest['name']]['fx'].add(k)

                    elif nest['mode']=='rep':
                        if nest['name'] in rep.repl:
                            repf[nest['name']]=nest
                        else:
                            nf=True
                            if not nest['name'] in coinc.keys():
                                coinc[nest['name']]={}
                            coinc[nest['name']].update({'rep':set()})
                            for k in repl:
                                if (nest['name'] in k) or (k in nest['name']):
                                    coinc[nest['name']]['rep'].add(k)
                    
                fxf.append(s)
            
            vj.fxf=fxf
            vj.repf=repf
            print(fxf)
            print(repf)
            if nf:
                
                print('funciones no encontradas')
                for k in coinc:
                    print('- "{funcion}"'.format(funcion=k))
                    print('  coincidencias:')
                    for l in coinc[k]:
                        print("  -tipo {l}: {f}".format(l=l,f=', '.join(coinc[k][l])))
            vj.process()


    def process():
        """
        tengo la idea de que los procesos del reproductor deben de ser threads paralelos
        los threads consecutivos eliminan al anterior

        pues pipas que más?
        creo que los procesos en paralelo deben de morir o sobreescribirse si 
        """
        for r in vj.repf:
            Thread(target=rep.repl[r],args=vj.repf[r]['args'],name=r).start()
            
                
"""

cap=cv2.VideoCapture('salida.mp4')
fps=cap.get(cv2.CAP_PROP_FPS)

"""
vj.init()
