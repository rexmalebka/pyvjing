# pyvjing

## VJing with opencv + python

What does this program do? 

- Interpreter
- Processer
- loopable video player
- video player functions
- computer vision effects

### ideas

This changes the normal funcion of the video player, pause, end, choose the loop intervals, etc;
the language have this form:

```
#function param1,param2,paramN
```

This changes the frames, adding computer vision effects, changing colorspaces, separating colors, etc;
```
effect param1,param2,paramN
```

It could be possible to make effects over the same frame like this:
```
effect1( effect2( effect3( frame)))
```

The result of affecting a frame it's put on anothe effect and so on.
```
effect1 | effect 2 | effect 3
```

It is possible to make a loop of effects affecting different frames (not neccesarry on the same loop of frames)
```
effect1 & effect2 & effect3
```
to this result:
```
effect1(frame1) , effect2(frame2) , effect3(frameN) 
```

it's still not implemented, but would be cool to mix this things together
```
effect1 & effect 2 | effect 3 & effect 4
```

to this:
```
effect1(frame1) , effect(frame2) , effect3( effect4( frameN))
```

But, I'm still a newbie, sorry.

### Future Features

- buffer alocation for certain loops (not so big)



