import matplotlib.pyplot as plt
import numpy as np

with open('buffertimes.log','r') as bt:
    bt=bt.read()
with open('lectura.log','r') as lt:
    lt=lt.read()

buf=np.array([float(k.split(' ')[1])  for k in bt.split('\n')[:-1]])[:200]
lec=np.array([float(k.split(' ')[1])  for k in lt.split('\n')[:-1]])[:200]
print(buf.shape)
print(lec.shape)
plt.figure('tiempos de carga y lectura de cuadros de un buffer de 200 cuadros')
plt.plot(np.cumsum(buf),label='tiempo de carga de buffer de video')
plt.plot(np.cumsum(lec),label='tiempo de lectura de cuadros')
plt.xlabel('cuadros')
plt.ylabel('tiempo')
#a.le
#plt.plot(np.cumsum(lec)*np.cumsum(buf),label='tiempo carga * tiempo lectura')

plt.legend(loc='best')
plt.show()
