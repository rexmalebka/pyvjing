import cv2
import numpy as np
from threading import enumerate as enumthreads
from multiprocessing import Pipe
from time import sleep as sleeptime
"""
añada su función en def, considere los argumentos a utilizar, recuerde que los datos se envian por pipas
añada las pipas de comunicación que necesite
agrege su función en fxl
"""

class fx():
    #lista de funciones fx
    fxl=[
            'blur',
            'canny',
            'inrange',
            'roi',
            'line',
            'circle',
            'rect',
            'text'
            ]

    #las funciones tienen un pipe de entrada y un pipe de salida que se conectan entre sí.

    pipes={
            }
        #aqui defino las pipas de comunicacion necesarias

    def load(*asdf):
        #aqui defino una por una las funciones
        pass
    print('riñon cargado')
    pass

class rep():
    #lista de funciones rep
    
    class pipes():
        """
        this class generates pipes for comunication
        
        nfunction=Pipe()
        nfuncion=[parentPipe,childPipe]
        """
        pause=Pipe()
        end=Pipe()
        load=Pipe()
        
        #initialize necessary pipes
        def init(*_,**__):
            """
            this function initialize pipes with default values avoiding blocking the system
            """
            rep.pipes.pause[0].send(False)
            rep.pipes.end[0].send(False)
            rep.pipes.load[0].send(None)

    def load(*asdf,**__):
        """
        this function loads the buffer of a video from a certain index, it also comunicates with the show function
        """
        cap=cv2.VideoCapture(name)
        totalframes=int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        #calculo el total de indices:
        inicio=50
        fin=1
        paso=-10
        indx=np.arange(inicio,fin,paso,dtype=[np.int32,np.int16][2**16>totalframes])
        while indx.shape[0]<clase.bufsize:
            indx=np.concatenate((indx,indx))

        indx=indx[:clase.bufsize]
        if paso>0:
            cap.set(1,indx[0]-1)
        else:
            cap.set(1,indx[clase.bufsize-1]-1)
        frames=np.array([cap.read()[1]])
        cap.set(1,cap.get(1)-1)
        while True:
            #log=open('buffertimes.log','a')
            #envio la señal para que la ventana de video se espere a que le llegue nuevos videos
            showchild.send(True)
            for i in indx[:clase.bufsize][::int(paso/abs(paso))]:
                h,frame=cap.read()
                if not h and cap.get(1)==totalframes:
                    #significa que llegó a su fin de lectura
                    cap.set(1,inicio-1)

                while i!=cap.get(1):
                    if cap.get(1)>i:
                        if paso>0:
                            cap.set(1,inicio-1)
                        else:
                            cap.set(1,fin-1)
                    h,frame=cap.read()

                if i==cap.get(1):
                    frames=np.concatenate((frames,np.array([frame])))
            
            if paso>0:
                clase.framesb=frames
            else:
                clase.framesb=frames[::-1]
        
            #envia la señal para desbloquear la ventana de videos
            showchild.send(False)
            indx=np.roll(indx,[-clase.bufsize,clase.bufsize][paso<0])
            #se mantiene ala espera de peticiones de lectura
            while not bufferparent.recv():
                pass
        #mueve el indice 100 posiciones
        
        pass

    def pause(*_,**__):
        """
        this function toogle the value from the pause parent value
        in pause parent:
          for True pause is on
          for False pause is off
        """
        valor=rep.pipes.pause[1].recv()
        rep.pipes.pause[0].send(not valor)
        if valor:
            print("pause is on")
        else:
            print("pause is off")

    def end(*_,**__):
        """
        this function ends it all
        in end parent
        for True end it all
        for False pass it all
        """
        print("ended.")
        rep.pipes.end[0].send(True)

    def sleep(time,*_):
        time=float(time)
        print("wait {time} seconds.".format(time=time))
        rep.pipes.pause[0].send(True)
        sleeptime(time)
        rep.pipes.pause[0].send(False)
        pass
    def proc(*_,**__):
        print("active threads:")
        for k in enumthreads():
            print("    "+k.name)
    
    repl={
            'load':load,
            'pred':None,
            'fx':None,
            'pause':pause,
            'loop':None,
            'show':None,
            'end':end,
            'sleep':sleep,
            'proc':proc
            }







#[[{'mode': 'fx', 'name': 'canny', 'args': ['5', '5']}], [{'mode': 'fx', 'name': 'blur', 'args': ['5', '5']}]]

#load.child=leer
#mostrar->
#canny.child=load.parent
#blur.child =canny.parent

#canny.child=load.parent
#blur.
class osc():
    oscl=[]
    pass

rep.pipes.init()
